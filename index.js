#! /usr/bin/env node

"use strict";

const os = require('os'),
      path = require('path'),
      fs = require('fs'),
      execFile = require('child_process').execFile;

const SHELL = 'ios-icon-generator.sh'

let argv = process.argv.splice(2);

if( os.platform() === 'win32' ) {
  console.log( 'only macOS is supported!' )
}

const global_cli_path = path.join( process.cwd(), 'node_modules', 'ios-icon-generator' ) ;

let file = path.join( global_cli_path, SHELL);

if( !fs.existsSync( file ) ) {
  file = path.join( __dirname, SHELL)
}

execFile( file, argv, (error, stdout, stderr) => {
    if (error ) {
      if( !(error.code && error.code >= 0) ) {
        console.error(error)
      }
    }
    console.log(stdout);
});
