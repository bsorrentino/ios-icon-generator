# iOS/macOS/watchOS APP ICONS utilities

## install
```
npm i https://bsorrentino@bitbucket.org/bsorrentino/ios-icon-generator.git -g
```
## commands

### ios-icon-generator
It is a shell script which aim to generate iOS/macOS/watchOS APP icons easier and simply.
> cloned from [here](https://github.com/smallmuou/ios-icon-generator/)

#### Usage:
```
ios-icon-generator <srcfile> <dstpath>
```

#### Refer
* [iOS Icons Size](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/app-icon/)
* [macOS Icons Size](https://developer.apple.com/design/human-interface-guidelines/macos/icons-and-images/app-icon/)
* [watchOS Icons Size](https://developer.apple.com/design/human-interface-guidelines/watchos/icons-and-images/home-screen-icons/)


### icns2png

This is a command line script that converts `.icns` files to `.png` files.
> inspired by [here](https://github.com/louisdh/icns2png)

#### Usage
```
icns2png <icns file without ext>
```
